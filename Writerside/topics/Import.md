# Import of CSV Files

## Konvert test from .sql data to .csv

Ich habe meine test daten Chat-GPT gegeben um sie konvertieren.
<br>
**Die neuen test Daten:**

``` 
name,beschreibung,Startdatum,Enddatum,status
Wikizz,"Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",2009-05-13,2023-07-28,geplant
Vidoo,"Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.",2006-03-08,2023-03-26,abgeschlossen
Leexo,"Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl. Nunc nisl.",2005-03-31,2024-01-15,geplant
Katz,Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.,2016-11-25,2024-07-29,abgeschlossen
Flipbug,"Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.",2017-07-22,2024-03-16,abgeschlossen
Fatz,"Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.",2008-07-02,2023-06-24,geplant
Dynava,Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.,2005-06-05,2024-10-15,geplant
```

## Einfügen in Datagrip

Dannach bin ich in Datagrip gegangen, habe den table in dem ich was hinzufügen will gerechtsklickt und habe dann import ausgewählt. Danach habe ich das file ausgewählt und nur noch ausgewählt das die erste row der header ist.



